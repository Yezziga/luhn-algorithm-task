package com.experis;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private final Main main = new Main();

    @Test
    void checkDouble() {
        assertEquals(4, main.doubleNum(2));
        assertEquals(12, main.doubleNum(6));
    }

    @Test
    void testUserInputSucceed() {
        assertTrue(main.correctUserInput("1234567890123456"));
        assertTrue(main.correctUserInput("4242424242424242"));
        assertTrue(main.correctUserInput("424242424242"));

    }

    @Test
    void testUserInputFail() {
        assertFalse(main.correctUserInput("1234567890123456j"));
        assertFalse(main.correctUserInput("1234567890123456+"));
        assertFalse(main.correctUserInput("12345678 0123456"));
        assertFalse(main.correctUserInput("12345678\"0123456"));
        assertFalse(main.correctUserInput(""));
    }

    @Test
    void testSeparateCheckDigitSucceed() {
        assertEquals("424242424242424 2", main.separateCheckDigit("4242424242424242"));
    }

    @Test
    void testLuhnSucceed() {
        String[] strArr = main.luhnAlgorithm("6767").split(":");
        String value = strArr[1];
        assertEquals("Valid", value);
    }

    @Test
    void testLuhnFail() {
        String[] strArr = main.luhnAlgorithm("67677").split(":");
        String value = strArr[1];
        assertEquals("Not valid", value);
    }
}