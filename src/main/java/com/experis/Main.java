package com.experis;

import java.util.Scanner;

public class Main {

    /**
     * Multiplies the given number with 2 and returns it.
     * @param num the number to double
     * @return the number multiplied with 2
     */
    public int doubleNum(int num) {
        return num*2;
    }

    /**
     * Checks if the given string only contains numbers.
     * @param number the user input
     * @return true if string only contains numbers, else false
     */
    public boolean correctUserInput(String number) {
        String regex = "[0-9]+";

            if(number.matches(regex)){
                return true;
            }

        return false;
    }


    /**
     * The luhn algorithm which calculates the check digit and
     * validates whether or not the given number is valid.
     * The output of this method will look something like the following:
     * "7:true" the number being the calculated check digit and 'true' being the
     * validation, indicating that the number is valid (or not)
     * @param num the number to valiate
     * @return a String containing the expected check digit and "valid"/"not valid"
     */
    public String luhnAlgorithm(String num) {
        int nDigits = num.length()-1; // exclude the right-most character (check digit)
        int provided = Character.getNumericValue(num.charAt(num.length()-1)); // convert char to int

        int nSum = 0;
        boolean isSecond = true;

        //
        for (int i = nDigits - 1; i >= 0; i--) {
            int d = Character.getNumericValue(num.charAt(i)); // convert char to int

            // double every second digit
            if (isSecond == true) {
                d = doubleNum(d);
                if(d>9) {
                    d-=9;
                }
            }
            nSum+=d;

            // Every other digit
            isSecond = !isSecond;
        }

        // calculate the expected check digit
        int expected = (nSum*9) % 10;
        String valid;
        if(expected==provided){
            valid = "Valid";
        } else {
            valid = "Not valid";
        }

        return expected + ":" + valid;

    }

    /**
     * Checks whether or not the given number has the same
     * length as a credit card (16 digits)
     * @param num the number to check
     * @return true if number's length is 16, else false
     */
    public boolean isCreditCard(String num) {
        return num.length() == 16;
    }

    /**
     * Separates the right-most character from the rest with a space
     * and returns the new string
     * @param num the string to alter
     * @return a new string but with a space before the last character
     */
    public String separateCheckDigit(String num) {
        String newString = num.substring(0, num.length()-1);
        newString+= " " + num.charAt(num.length()-1);
        return newString;
    }

    /**
     * Reads the input from the console and returns it
     * @return the input from the console
     */
    public String promptForInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    /**
     * Fetches the information needed to print the users input, check digit provided
     * and expected, whether or not the checksum is valid and the number of digits.
     * Then prints out the information to the console
     * @param input the number to fetch information about
     */
    public void printInfo(String input) {
        String inputStr = "Input: " + separateCheckDigit(input) + "\n";
        String[] strArr = luhnAlgorithm(input).split(":");
        String provided = "Provided: " + input.charAt(input.length()-1) + "\n";
        String expected = "Expected: " + strArr[0] + "\n";
        String checksum = "\nChecksum: " + strArr[1] + "\n";
        String creditCard = "Digits: " + input.length();
        if(isCreditCard(input)) {
            creditCard+= " (credit card)";
        }
        System.out.println(inputStr + provided + expected + checksum + creditCard);
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println("Enter a code (digits only)");
        String input = main.promptForInput();
        // proceed only if input is in correct format (numbers only)
        if(main.correctUserInput(input)) {
                main.printInfo(input);
        }
    }
}